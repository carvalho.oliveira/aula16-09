#!/bin/bash

echo 'A variável $0 exibe o nome do script:' $0
sleep 2
echo 'A variável $1 exibe o primeiro argumento passado na linha de comando:' $1
sleep 2
echo 'A variável $2 exibe o segundo argumento passado na linha de comando:' $2
sleep 2
echo 'A variável $# exibe a quantidade de argumentos digitados na linha de comando:' $#
sleep 2
echo 'A variável $* exibe todos os argumentos passados na linha de comando:' $*

